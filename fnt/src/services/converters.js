export function fromDifficulty(obj) {
	const mapping = [
		'simple',
		'medium',
		'hard'
	]
	return mapping[obj - 1]
}

export function fromTemplete(obj) {
	return {
		id: obj.id,
		language: obj.language,
		difficulty: fromDifficulty(obj.difficulty),
		description: obj.description,
		steps: obj.steps,
	}
}

export function fromCourse(obj) {
	return {
		id: obj.id,
		language: obj.language,
		difficulty: fromDifficulty(obj.difficulty),
		description: obj.description,
		startDate: new Date(obj.start_date),
		endDate: new Date(obj.end_date),
		isApproved: obj.is_approved,
	}
}

export function fromGroup(obj) {
	return {
		language: obj.language,
		difficulty: fromDifficulty(obj.difficulty),
		groups: obj.groups,
		startDate: new Date(obj.start_date),
		endDate: new Date(obj.end_date),
	}
}

export function fromGroupStudent(obj) {
	return {
		id: obj.id,
		name: obj.name,
		surname: obj.surname,
		patronymic: obj.patronymic,
		email: obj.email,
		isApproved: obj.is_approved,
		isDeleted: obj.is_deleted,
	}
}

export function fromUserData(obj) {
	return {
		userId: obj.user_id,
		userEmail: obj.user_email,
		userName: obj.user_name,
		userSurname: obj.user_surname,
		userPatronymic: obj.user_patronymic,
		accessToken: obj.access_token,
		tokenType: obj.token_type,
		roleId: obj.role_id,
	}
}

export function fromDict(obj) {
	return {
		id: obj.id,
		name: obj.name,
	}
}
