import { Resources } from '@/services/enums';

function isAuthenticated() {
	const storage = localStorage.vuex ? JSON.parse(localStorage.vuex) : false

	return storage && !!storage.user.token
}


function hasAccessToResource(resourceId) {
	const storage = localStorage.vuex ? JSON.parse(localStorage.vuex) : false

	if (storage && storage.user) {
		const permission = storage.user.permissions.find((p) => p.id === resourceId)
		return permission !== undefined
	}
	return false
}


export function authGuard (to, from, next) {
  if (to.name !== 'login' && !isAuthenticated()) {
		next({ path: '/login' })
	} else if (to.name === 'login' && isAuthenticated()) {
		next({ path: '/courses' })
	} else {
		next()
	}
}

export function permissionsGuard (to, from, next) {
	const { resource } = to.meta
	if (hasAccessToResource(resource)) {
		next()
	} else if (hasAccessToResource(Resources.COURSES)) {
		next({ name: 'courses' })
	}
}
