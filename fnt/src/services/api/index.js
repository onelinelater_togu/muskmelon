import axios from 'axios'
import { fromDict, fromUserData } from '@/services/converters';
import TemplatesResources from '@/services/api/templates';
import CoursesResources from '@/services/api/courses';
import GroupsResources from '@/services/api/groups';


const storage = localStorage.vuex ? JSON.parse(localStorage.vuex) : false

if (storage && storage.user) {
	axios.defaults.headers.common = {
		'Authorization': `Bearer ${storage.user.token}`,
	};
}


class ApiResources {
	API_HOST = 'http://localhost:8000'

	templates = new TemplatesResources(this)
	courses = new CoursesResources(this)
	groups = new GroupsResources(this)

	async getPermissions(roleId) {
		const response = await axios.get(`${this.API_HOST}/permissions?role_id=${roleId}`)
		return response.data.map(fromDict)
	}

	async login(email, password) {
		const response = await axios.post(`${this.API_HOST}/token`, { email, password })
		const userData = fromUserData(response.data)
		axios.defaults.headers.common = {
			'Authorization': `Bearer ${userData.accessToken}`,
		};
		return userData
	}

	async logout() {
		axios.defaults.headers.common = {
			'Authorization': undefined,
		};
		localStorage.clear();
		sessionStorage.clear();
	}
}

const api = new ApiResources()
export default function UseApi () {
	return api
}
