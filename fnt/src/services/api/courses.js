import axios from 'axios';
import { fromCourse } from '@/services/converters';

export default class CoursesResources {
	API_HOST

	constructor(api) {
		this.API_HOST = api.API_HOST
	}

	async addCourse(templateId, dateStart, dateEnd) {
		return axios.post(`${this.API_HOST}/courses`, {
			template_id: templateId,
			start_date: dateStart,
			end_date: dateEnd,
		})
	}

	async getCourses() {
		const res = await axios.get(`${this.API_HOST}/courses`)
		return res.data.map(fromCourse)
	}

	async subscriptCourse(courseId) {
		await axios.post(`${this.API_HOST}/course/${courseId}/subscript`)
	}
}
