import axios from 'axios';
import { fromGroup, fromGroupStudent } from '@/services/converters';

export default class GroupsResources {
	API_HOST

	constructor(api) {
		this.API_HOST = api.API_HOST
	}

	async getGroups() {
		const res = await axios.get(`${this.API_HOST}/groups`)
		return res.data.map(fromGroup)
	}

	async getStudents(groupId) {
		const res = await axios.get(`${this.API_HOST}/groups/${groupId}/students`)
		return res.data.map(fromGroupStudent)
	}

	async approveStudent(studentId, groupId) {
		return axios.post(`${this.API_HOST}/approve-student`, {
			student_id: studentId,
			group_id: groupId
		})
	}

	async exileStudent(studentId, groupId) {
		return axios.post(`${this.API_HOST}/exile-student`, {
			student_id: studentId,
			group_id: groupId
		})
	}

	async restoreStudent(studentId, groupId) {
		return axios.post(`${this.API_HOST}/restore-student`, {
			student_id: studentId,
			group_id: groupId
		})
	}
}
