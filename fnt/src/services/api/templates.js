import axios from 'axios';
import { fromTemplete } from '@/services/converters';

export default class TemplatesResources {
	API_HOST

	constructor(api) {
		this.API_HOST = api.API_HOST
	}

	async getTemplates() {
		const res = await axios.get(`${this.API_HOST}/templates`)
		return res.data.map(fromTemplete)
	}

	async removeTemplate(index) {
		return axios.delete(`${this.API_HOST}/templates/${index}`)
	}

	async addTemplate(file) {
		return axios.post(`${this.API_HOST}/templates`, {file: file})
	}
}
