import { mapActions, mapState } from 'vuex';
import Avatar from "vue-boring-avatars";

export default {
	name: 'UserTools',
	components: {
		Avatar
	},
	computed: {
		...mapState({
			userName: state => state.user.userName,
			userSurname: state => state.user.userSurname,
			userPatronymic: state => state.user.userPatronymic,
		}),
		name () {
			return `${this.userSurname} ${this.userName ? this.userName[0] : ''}.`
		}
	},
	data() {
		return {
			open: false
		};
	},
	methods: {
		...mapActions({pushUserData: 'user/pushUserData'}),
		async logout() {
			await this.$api.logout();
			this.$router.push({path: 'login'})
		}
	}
};