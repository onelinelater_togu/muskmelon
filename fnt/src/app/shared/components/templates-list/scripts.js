import TemplateAddModal from '@/app/shared/components/template-add-modal/TemplateAddModal';
import CourseAddModal from '@/app/shared/components/course-add-modal/CourseAddModal';
import { mapActions } from 'vuex';

export default {
	name: 'templates-list',
	data() {
		return {
			list: [],
			loading: true,
		};
	},
	computed: {
	},
	components: {
		TemplateAddModal,
		CourseAddModal,
	},
	async mounted() {
		await this.reload();
	},
	methods: {
		...mapActions('templates', [
			'toggleAddTemplateModal'
		]),
		...mapActions('courses', [
			'toggleAddCourseModal'
		]),
		toggleAddTemplate() {
			this.toggleAddTemplateModal()
		},
		toggleAddCourse(event, templateId) {
			this.toggleAddCourseModal(templateId)
		},
		async reload() {
			this.loading = true;
			this.list = await this.$api.templates.getTemplates();
			this.loading = false;
		},
		async confirmRemoval(event, index) {
			const result = await this.$confirm({
				message: 'Вы действительно хотите удалить шаблон?',
				acceptText: 'Да',
				cancelText: 'Отмена'
			})
			if (result) {
				await this.$api.templates.removeTemplate(index)
				await this.$alert('Шаблон успешно удалён');
				await this.reload()
			}
		}
	}
}