import { mapActions, mapState } from 'vuex';

export default {
	name: 'CourseAddModal',
	computed: {
		...mapState({
			openCourseImportModal: state => state.courses.openCourseImportModal,
			templateId: state => state.courses.templateId,
		}),
		isDateFilled() {
			return this.date && this.date.length === 2
		},
	},
	data() {
		return {
			date: []
		};
	},
	async mounted() {
	},
	methods: {
		async onConfirm() {
			await this.$api.courses.addCourse(this.templateId, this.date[0], this.date[1])
			this.toggle()
		},
		...mapActions({toggleAddCourse: 'courses/toggleAddCourseModal'}),
		toggle() {
			this.toggleAddCourse()
		}
	}
}