import { mapState } from 'vuex';


const groupTypes = [
  {
    text: 'Студенты',
		type: 'activeStudents',
  },
  {
    text: 'Заявки',
		type: 'requestedStudents',
  },
  {
    text: 'Отчисленные',
		type: 'deletedStudents',
  }
];


export default {
  name: 'groups-list',
  data() {
    return {
      list: [],
			students: [],
			groupTypes,
			loading: true,
			studentsLoading: false,
			selectedGroupType: 0,
			selectedGroup: null
    };
  },
	computed: {
		...mapState({
			roleId: state => state.user.roleId,
		}),
		studentsType() {
			const groups = {
				activeStudents: this.activeStudents,
				requestedStudents: this.requestedStudents,
				deletedStudents: this.deletedStudents,
			}
			return groups[groupTypes[this.selectedGroupType].type]()
		}
	},
	async mounted() {
		await this.reload()
	},
	methods: {
		async reload() {
			this.loading = true
			this.list = await this.$api.groups.getGroups()
			this.students = []
			this.loading = false
		},
		async fetchStudents(events, groupId) {
			this.studentsLoading = true
			this.selectedGroup = groupId
			this.students = await this.$api.groups.getStudents(groupId)
			this.studentsLoading = false
		},
		activeStudents() {
			return this.students.filter((items) => items.isApproved && !items.isDeleted)
		},
		requestedStudents() {
			return this.students.filter((items) => !items.isApproved && !items.isDeleted)
		},
		deletedStudents() {
			return this.students.filter((items) => items.isDeleted)
		},
		async approveStudent(event, studentId) {
			const res = await this.$confirm({
        message: 'Вы действительно хотите зачислить студента?',
        acceptText: 'Да',
        cancelText: 'Отмена'
      })
			if (res) {
				await this.$api.groups.approveStudent(studentId, this.selectedGroup)
				await this.reload()
			}
		},
		async exileStudent(event, studentId) {
			const res = await this.$confirm({
        message: 'Вы действительно хотите отчислить студента?',
        acceptText: 'Да',
        cancelText: 'Отмена'
      })
			if (res) {
				await this.$api.groups.exileStudent(studentId, this.selectedGroup)
				await this.reload()
			}
		},
		async restoreStudent(event, studentId) {
			const res = await this.$confirm({
        message: 'Вы действительно хотите восстановить студента?',
        acceptText: 'Да',
        cancelText: 'Отмена'
      })
			if (res) {
				await this.$api.groups.restoreStudent(studentId, this.selectedGroup)
				await this.reload()
			}
		}
	},
}