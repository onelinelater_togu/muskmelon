export default {
	name: 'IconComponent',
	components: {},
	props: {
		name: {
			type: String,
		},
		width: {
			type: Number,
			default: 15,
		},
		height: {
			type: Number,
			default: 15,
		},
	},
};