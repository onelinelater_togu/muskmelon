import IconComponent from '@/app/shared/components/icon/IconComponent';
import { mapActions, mapState } from 'vuex';

export default {
	name: 'LoginForm',
	components: {
		IconComponent,
	},
	computed: {
		...mapState({
			userName: state => state.user.userName,
			roleId: state => state.user.roleId,
		})
	},
	data() {
		return {
			title: 'Muskmelon',
			email: '',
			password: ''
		};
	},
	methods: {
		...mapActions({pushUserData: 'user/pushUserData'}),
		...mapActions({setPermissions: 'user/setPermissions'}),
		...mapActions({setToken: 'user/setToken'}),
		async loginUser () {
			try {
				const userData = await this.$api.login(this.email, this.password);
				const permissions = await this.$api.getPermissions(userData.roleId)
				this.pushUserData(userData)
				this.setPermissions(permissions)
				this.setToken(userData.accessToken)
				this.$router.push({path: 'courses'});
			} catch (error) {
				alert(error);
			}
		}
	}
};