import { mapActions, mapState } from 'vuex';
import { useEvent } from 'balm-ui';

export default {
	name: 'TemplateAddModal',
	computed: {
		...mapState({
			openTemplateImportModal: state => state.templates.openTemplateImportModal,
		}),
		fileNames() {
			return this.files.map((item) => item.name)
		}
	},
	data() {
		return {
			balmUI: useEvent(),
			files: []
		};
	},
	async mounted() {
	},
	methods: {
		async onConfirm() {
			if (this.files.length > 0) {
				await this.$api.templates.addTemplate(await this.files[0].sourceFile.text())
				this.toggle()
				this.$emit('onTemplateAdd')
			}
		},
		...mapActions({toggleModal: 'templates/toggleAddTemplateModal'}),
		toggle() {
			this.toggleModal()
		}
	}
}