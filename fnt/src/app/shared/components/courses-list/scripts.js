import { mapState } from 'vuex';

export default {
  name: 'courses-list',
  data() {
    return {
      list: [],
			myList: [],
			loading: true,
    };
  },
	async mounted() {
		await this.reload()
	},
	computed: {
		...mapState({
			roleId: state => state.user.roleId,
		})
	},
	methods: {
		async reload() {
			this.loading = true
			this.list = await this.$api.courses.getCourses()
			if (this.isStudent()) {
				this.myList = this.list.filter((item) => [true, false].indexOf(item.isApproved) !== -1)
				this.list = this.list.filter((item) => [true, false].indexOf(item.isApproved) === -1)
			}
			this.loading = false
		},
		isStudent() {
			return parseInt(this.roleId) === 3
		},
		isTeacher() {
			return parseInt(this.roleId) === 2
		},
		async subscript(event, courseId) {
			this.list = await this.$api.courses.subscriptCourse(courseId)
			await this.reload()
		}
	}
}