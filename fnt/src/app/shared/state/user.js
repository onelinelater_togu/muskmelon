const tabs = [
	{
		icon: 'collections_bookmark',
		url: '/templates',
		text: 'Шаблоны',
		resourceId: 1,
	},
	{
		icon: 'library_books',
		url: '/courses',
		text: 'Курсы',
		resourceId: 2,
	},
	{
		icon: 'group',
		url: '/groups',
		text: 'Группы',
		resourceId: 3,
	},
	{
		icon: 'self_improvement',
		url: '/lectors',
		text: 'Преподаватели',
		resourceId: 4,
	}
];


export default {
	namespaced: true,
	state() {
		return {
			userId: '',
			userEmail: '',
			userName: 'a',
			userSurname: '',
			userPatronymic: '',
			roleId: 0,
			token: '',
			permissions: [],
			tabs: tabs,
		}
	},
	mutations: {
		saveUserData(state, userData) {
			state.userId = userData.userId
			state.userEmail = userData.userEmail
			state.userName = userData.userName
			state.userSurname = userData.userSurname
			state.userPatronymic = userData.userPatronymic
			state.roleId = userData.roleId
		},
		setPermissions(state, permissions) {
			state.permissions = JSON.parse(JSON.stringify(permissions))
		},
		setToken(state, token) {
			state.token = token
		},
	},
	getters: {
		permittedTabs(state) {
			if (state.permissions) {
				const permittedResourcesIds = state.permissions.map((item) => item.id)
				return tabs.filter((item) => permittedResourcesIds.indexOf(item.resourceId) !== -1)
			}
			return []
		}
	},
	actions: {
		pushUserData({commit}, userData) {
			commit('saveUserData', userData)
		},
		setPermissions({commit}, permissions) {
			commit('setPermissions', permissions)
		},
		setToken({commit}, token) {
			commit('setToken', token)
		}
	},
}
