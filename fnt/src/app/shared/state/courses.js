export default {
	namespaced: true,
	state() {
		return {
			openCourseImportModal: false,
			templateId: -1,
		}
	},
	mutations: {
		toggleCourseModal(state, templateId) {
			state.openCourseImportModal = !state.openCourseImportModal
			if (state.openCourseImportModal) {
				state.templateId = templateId
			} else {
				state.templateId = null
			}
		},
	},
	actions: {
		toggleAddCourseModal({commit}, templateId) {
			commit('toggleCourseModal', templateId)
		}
	},
}
