import { createStore } from 'vuex';
import TemplatesStore from './templates'
import CoursesStore from './courses'
import UserStore from './user'
import createPersistedState from 'vuex-persistedstate';


export const store = createStore({
	modules: {
		templates: TemplatesStore,
		courses: CoursesStore,
		user: UserStore,
	},
	plugins: [createPersistedState()]
})
