export default {
	namespaced: true,
	state() {
		return {
			openTemplateImportModal: false
		}
	},
	mutations: {
		toggleModal(state) {
			state.openTemplateImportModal = !state.openTemplateImportModal
		},
	},
	actions: {
		toggleAddTemplateModal({commit}) {
			commit('toggleModal')
		}
	},
}
