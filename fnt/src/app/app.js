import IconComponent from './shared/components/icon/IconComponent'
import UserToolsComponent from './shared/components/user-tools/UserTools'
import { mapGetters } from 'vuex';

export default {
	name: 'app',
	components: {
		IconComponent,
		UserToolsComponent,
	},
	data() {
		return {
			user: null,
			title: 'Muskmelon',
			active: 0
		};
	},
	computed: {
		...mapGetters('user', ['permittedTabs']),
	},
	watch: {
		active: function (val) {
			this.$router.go(this.tabs[val].url);
		}
	},
}
