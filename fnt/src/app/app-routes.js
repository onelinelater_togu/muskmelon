import { createWebHistory, createRouter } from 'vue-router';

import CoursesList from '@/app/shared/components/courses-list/courses-list';
import LoginForm from '@/app/shared/components/login-form/LoginForm';
import TemplatesList from '@/app/shared/components/templates-list/TemplatesList';
import GroupsList from '@/app/shared/components/groups-list/GroupsList';
import { authGuard, permissionsGuard } from '@/services/guards';
import { Resources } from '@/services/enums';

const appRoutes = [
	{ path: '/:pathMatch(.*)*',
		name: 'NotFound',
		redirect: '/courses',
		beforeEnter: [authGuard],
	},
	{
		path: '/login',
		name: 'login',
		component: LoginForm,
		beforeEnter: [authGuard],
	},
	{
		path: '/groups',
		name: 'groups',
		component: GroupsList,
		meta: { resource: Resources.GROUPS },
		beforeEnter: [authGuard, permissionsGuard],
	},
	{
		path: '/courses',
		name: 'courses-list',
		component: CoursesList,
		beforeEnter: [authGuard, permissionsGuard],
		meta: { resource: Resources.COURSES }
	},
	{
		path: '/templates',
		name: 'templates-list',
		component: TemplatesList,
		beforeEnter: [authGuard, permissionsGuard],
		meta: { resource: Resources.TEMPLATE }
	}
];

const routes = [...appRoutes];

const router = createRouter({
	history: createWebHistory(),
	routes
});


export default router;
