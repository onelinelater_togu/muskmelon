import { createApp } from 'vue'

import App from './app/app.vue';
import './main.scss';
import router from './app/app-routes'

import BalmUI from 'balm-ui';
import BalmUIPlus from 'balm-ui-plus';
import VueSvgInlinePlugin from 'vue-svg-inline-plugin';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'balm-ui-css';
import { store } from '@/app/shared/state';
import formatDateMixin from '@/app/shared/mixins/formatDate';
import UseApi from '@/services/api';

const app = createApp(App)

app.mixin(formatDateMixin)
app.use(router)
app.use(BalmUI, {
	$theme: {
		primary: '#327ee0',
		secondary: '#468701'
	}
});
app.use(VueSvgInlinePlugin);
app.use(store);
app.use(BalmUIPlus);
app.mount('#app')
app.config.globalProperties.$api = UseApi()
