from datetime import datetime, timedelta
from typing import List

from fastapi import APIRouter

from data.db import Resources, Permissions
from domain.schemas import LoginUserSchema, UserSchema, DictSchema
from presentation.routes.utils import get_db, authenticate_user, create_access_token, JWTBearer

from fastapi import Depends, HTTPException, status

router = APIRouter()


@router.post('/token', response_model=UserSchema)
async def login(user_data: LoginUserSchema, db=Depends(get_db)):
    user = authenticate_user(db, user_data.email, user_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=60)
    access_token = create_access_token(
        data={'sub': str(user.id)}, expires_delta=access_token_expires
    )

    return dict(
        user_id=user.id,
        user_email=user.email,
        user_name=user.name,
        user_surname=user.surname,
        user_patronymic=user.patronymic,
        role_id=user.role_id,
        access_token=access_token,
        token_type='bearer'
    )


@router.get('/permissions', response_model=List[DictSchema], dependencies=[Depends(JWTBearer())])
async def get_permissions(role_id: int, db=Depends(get_db)):
    resources = db.query(
        Resources.id.label('id'),
        Resources.name.label('name'),
    ).select_from(Permissions).filter(Permissions.role_id == role_id).join(Resources).all()

    return resources
