from typing import List
from fastapi import Depends, APIRouter

from data.db import Template, Course
from data.db.entities.courses import CourseRequests, StudentsGroups
from domain import schemas
from domain.schemas import CreateCourseSchema
from presentation.routes.utils import get_db, get_current_user

router = APIRouter()


@router.get("/courses", response_model=List[schemas.CoursesSchema])
def list_courses(db=Depends(get_db), current_user=Depends(get_current_user)):
    if not current_user:
        return db.query(
            Course.id,
            Template.language,
            Template.description,
            Template.difficulty,
            Course.start_date,
            Course.end_date,
            Course.template_id,
        ).join(Template).all()
    elif current_user.role_id == 2:
        return db.query(
            Course.id,
            Template.language,
            Template.description,
            Template.difficulty,
            Course.start_date,
            Course.end_date,
            Course.template_id,
        ).join(Template).outerjoin(StudentsGroups).filter(
            StudentsGroups.teacher_id == current_user.id
        ).all()
    elif current_user.role_id == 3:
        return db.query(
            Course.id,
            Template.language,
            Template.description,
            Template.difficulty,
            Course.start_date,
            Course.end_date,
            Course.template_id,
            CourseRequests.is_approved,
        ).join(Template).outerjoin(StudentsGroups).outerjoin(CourseRequests).all()


@router.post('/courses')
def create_course(new_course_data: CreateCourseSchema, db=Depends(get_db)):
    new_template = Course(
        template_id=new_course_data.template_id,
        start_date=new_course_data.start_date,
        end_date=new_course_data.end_date,
    )
    db.add(new_template)
    db.commit()

    return 200


@router.post('/course/{course_id}/subscript')
def create_course(course_id, db=Depends(get_db), current_user=Depends(get_current_user)):
    group = db.query(StudentsGroups).filter(StudentsGroups.course_id == course_id).first()
    if current_user.role_id == 2:
        return 400
    if not group:
        group = StudentsGroups(
            course_id=course_id,
            teacher_id=2,
        )
        db.add(group)
        db.flush()
    group_id = group.id
    existed_request = db.query(CourseRequests).filter(
        CourseRequests.student_id == current_user.id,
        CourseRequests.group_id == group_id,
    ).first()
    if existed_request:
        return 302
    new_request = CourseRequests(
        student_id=current_user.id,
        group_id=group_id
    )
    db.add(new_request)
    db.commit()

    return 200
