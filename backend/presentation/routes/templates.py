from typing import List
import json
from fastapi import Depends, APIRouter
from sqlalchemy import func

from data.db import Template, TemplateStep
from domain import schemas
from domain.schemas import TemplateImportSchema
from presentation.routes.utils import get_db, JWTBearer

router = APIRouter()


@router.get('/templates', dependencies=[Depends(JWTBearer())], response_model=List[schemas.TemplateSchema])
def list_templates(db = Depends(get_db)):
    templates = db.query(
        Template.id,
        Template.language,
        Template.difficulty,
        Template.description,
        Template.source,
        Template.source_type,
        func.array_agg(TemplateStep.description).label('steps'),
    ).join(Template).group_by(
        Template.id,
    ).all()

    return templates


@router.post('/templates', dependencies=[Depends(JWTBearer())])
def list_templates(args: TemplateImportSchema, db=Depends(get_db)):
    new_template_data = json.loads(args.file)

    diff = dict(
        simple=1,
        medium=2,
        hard=3,
    )
    new_template = Template(
        language=new_template_data['language'],
        description=new_template_data['description'],
        source='file',
        source_type=1,
        difficulty=diff[new_template_data['difficulty']],
    )
    db.add(new_template)
    db.flush()
    for step in new_template_data['steps']:
        db.add(TemplateStep(
            template_id=new_template.id,
            description=step,
        ))
    db.commit()

    return 200


@router.delete("/templates/{template_id}", dependencies=[Depends(JWTBearer())])
def remove_template(template_id: int,  db=Depends(get_db)):
    steps = db.query(TemplateStep).filter(TemplateStep.template_id == template_id)
    steps.delete()
    template = db.query(Template).filter(Template.id == template_id).first()
    db.delete(template)
    db.commit()

    return 200
