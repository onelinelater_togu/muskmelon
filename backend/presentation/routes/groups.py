from typing import List
from fastapi import Depends, APIRouter
from sqlalchemy import func
from sqlalchemy.sql.elements import and_

from data.db import Template, Course, Users
from data.db.entities.courses import CourseRequests, StudentsGroups
from domain.schemas import ApproveSchema
from presentation.routes.utils import get_db, get_current_user

router = APIRouter()


@router.get('/groups', response_model=List)
def list_courses(db=Depends(get_db), current_user=Depends(get_current_user)):
    courses = {
        item[0]: (item[1], item[2], item[3], item[4]) for item in db.query(
            Course.id, Template.language, Template.difficulty, Course.start_date, Course.end_date
        ).join(Template).all()
    }
    groups = db.query(
        func.min(StudentsGroups.teacher_id),
        func.array_agg(StudentsGroups.id),
        StudentsGroups.course_id,
    ).filter(StudentsGroups.teacher_id == current_user.id).group_by(StudentsGroups.course_id).all()

    return [
        dict(
            groups=item[1],
            language=courses[item[2]][0],
            difficulty=courses[item[2]][1],
            start_date=courses[item[2]][2],
            end_date=courses[item[2]][3],
        )
        for item in groups
    ]


@router.get('/groups/{group_id}/students', response_model=List)
def group_students(group_id, db=Depends(get_db)):
    groups = db.query(
        Users.id,
        Users.name,
        Users.surname,
        Users.patronymic,
        Users.email,
        CourseRequests.is_approved,
        CourseRequests.is_deleted,
    ).filter(CourseRequests.group_id == group_id).join(Users).all()

    return groups


@router.post('/approve-student')
def approve_students(args: ApproveSchema, db=Depends(get_db)):
    request = db.query(CourseRequests).filter(
        and_(
            CourseRequests.group_id == args.group_id,
            CourseRequests.student_id == args.student_id
        )
    ).first()
    request.is_approved = True
    db.commit()

    return 200


@router.post('/exile-student')
def approve_students(args: ApproveSchema, db=Depends(get_db)):
    request = db.query(CourseRequests).filter(
        and_(
            CourseRequests.group_id == args.group_id,
            CourseRequests.student_id == args.student_id
        )
    ).first()
    request.is_deleted = True
    db.commit()

    return 200


@router.post('/restore-student')
def approve_students(args: ApproveSchema, db=Depends(get_db)):
    request = db.query(CourseRequests).filter(
        and_(
            CourseRequests.group_id == args.group_id,
            CourseRequests.student_id == args.student_id
        )
    ).first()
    request.is_deleted = False
    db.commit()

    return 200
