import datetime
from typing import List, Optional

from pydantic import BaseModel


class TemplateStepSchema(BaseModel):
    description: str

    class Config:
        orm_mode = True

class TemplateSchema(BaseModel):
    id: int
    language: str
    description: str
    source: str
    source_type: int
    difficulty: int
    steps: List[str]

    class Config:
        orm_mode = True


class TemplateImportSchema(BaseModel):
    file: str


class CreateCourseSchema(BaseModel):
    start_date: str
    end_date: str
    template_id: int


class CoursesSchema(BaseModel):
    id: int
    language: str
    description: str
    difficulty: int
    start_date: datetime.date
    end_date: datetime.date
    template_id: int
    is_approved: Optional[bool]

    class Config:
        orm_mode = True


class LoginUserSchema(BaseModel):
    email: str
    password: str


class UserSchema(BaseModel):
    user_id: int
    user_email: str
    user_name: str
    user_surname: str
    user_patronymic: str
    access_token: str
    token_type: str
    role_id: str


class DictSchema(BaseModel):
    id: int
    name: str

    class Config:
        orm_mode = True


class ApproveSchema(BaseModel):
    student_id: int
    group_id: int
