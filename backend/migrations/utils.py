import json

from alembic import op
from passlib.context import CryptContext
from werkzeug.security import generate_password_hash


def init_table_data(table, file_path):
    with open(file_path, 'rb') as file:
        data = json.loads(file.read().decode('utf8'))['data']
        op.bulk_insert(table, data)


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def init_users(table, file_path):
    with open(file_path, 'rb') as file:
        data = json.loads(file.read().decode('utf8'))['data']
        for item in data:
            item['password_hash'] = pwd_context.hash(item['password_hash'])
        op.bulk_insert(table, data)
