"""Add users, resourses, roles adn permissions

Revision ID: dca4e9f60d58
Revises: 4d94de7ed515
Create Date: 2022-11-21 13:59:03.790047

"""
from alembic import op
import sqlalchemy as sa
import os

# revision identifiers, used by Alembic.
from migrations.utils import init_table_data, init_users

revision = 'dca4e9f60d58'
down_revision = '4d94de7ed515'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    resources = op.create_table(
        'resources',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.Unicode(length=64), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    roles = op.create_table(
        'roles',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.Unicode(length=64), nullable=False),
        sa.Column('description', sa.Unicode(length=512), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    permissions = op.create_table(
        'permissions',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('role_id', sa.Integer(), nullable=False),
        sa.Column('resource_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['resource_id'], ['resources.id'], name='permissions_resource_id_fk', onupdate='CASCADE', ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['role_id'], ['roles.id'], name='permissions_role_id_fk', onupdate='CASCADE', ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    users = op.create_table(
        'users',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('email', sa.Unicode(length=256), nullable=False),
        sa.Column('name', sa.Unicode(length=64), nullable=False),
        sa.Column('surname', sa.Unicode(length=64), nullable=False),
        sa.Column('patronymic', sa.Unicode(length=64), nullable=False),
        sa.Column('password_hash', sa.Unicode(length=256), nullable=False),
        sa.Column('role_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['role_id'], ['roles.id'], name='role_id_fkey'),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('email')
    )
    init_table_data(resources, os.path.abspath('migrations/data/resources.json'))
    init_table_data(roles, os.path.abspath('migrations/data/roles.json'))
    init_users(users, os.path.abspath('migrations/data/users.json'))
    init_table_data(permissions, os.path.abspath('migrations/data/permissions.json'))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users')
    op.drop_table('permissions')
    op.drop_table('roles')
    op.drop_table('resources')
    # ### end Alembic commands ###
