"""Add template model and template step

Revision ID: bcae667abd6a
Revises: 
Create Date: 2022-11-12 14:25:07.269435

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'bcae667abd6a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        'template',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('language', sa.Unicode(length=32), nullable=False),
        sa.Column('description', sa.Unicode(length=128), nullable=False),
        sa.Column('source', sa.Unicode(length=128), nullable=False),
        sa.Column('source_type', sa.Integer(), nullable=False),
        sa.Column('difficulty', sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'template_step',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('template_id', sa.Integer(), nullable=True),
        sa.Column('description', sa.Unicode(length=128), nullable=False),
        sa.ForeignKeyConstraint(['template_id'], ['template.id'], name='template_id_fkey'),
        sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('template_step')
    op.drop_table('template')
    # ### end Alembic commands ###
