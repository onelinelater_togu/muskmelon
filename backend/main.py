from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from presentation.routes.templates import router as template_routes
from presentation.routes.courses import router as courses_routes
from presentation.routes.users import router as users_routes
from presentation.routes.groups import router as groups_routes
app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(template_routes)
app.include_router(courses_routes)
app.include_router(users_routes)
app.include_router(groups_routes)


@app.get("/")
async def root():
    return {"message": "Hello Bigger Applications!"}
