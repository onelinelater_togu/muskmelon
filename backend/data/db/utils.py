from sqlalchemy import Column, Integer, Sequence


def make_id_column(model, autoincrement: bool = True, seq_name: str = None) -> Column:
    if autoincrement:
        return Column(Integer, Sequence(seq_name, start=1, increment=1, metadata=model.metadata), primary_key=True)
    return Column(Integer, primary_key=True)
