from sqlalchemy import ForeignKey, Column, Date, Boolean, false

from data.db import Template
from data.db.database import Model
from data.db.utils import make_id_column


class Course(Model):
    __tablename__ = 'course'

    id = make_id_column(autoincrement=True, seq_name='course_id_seq', model=Model)
    template_id = Column(ForeignKey(Template.id, name='template_id_fkey'), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)


class CourseRequests(Model):
    __tablename__ = 'course_requests'

    id = make_id_column(autoincrement=True, seq_name='course_requests_id_seq', model=Model)

    student_id = Column(ForeignKey('users.id', name='student_id_fkey'), nullable=False)
    group_id = Column(ForeignKey('students_groups.id', name='students_groups_id_fkey'), nullable=False)
    is_approved = Column(Boolean, nullable=True, server_default=false())
    is_deleted = Column(Boolean, nullable=True, server_default=false())


class StudentsGroups(Model):
    __tablename__ = 'students_groups'

    id = make_id_column(autoincrement=True, seq_name='students_groups_id_seq', model=Model)
    course_id = Column(ForeignKey('course.id', name='course_id_fkey'))
    teacher_id = Column(ForeignKey('users.id', name='teacher_id_fkey'))
