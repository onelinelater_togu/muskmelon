from sqlalchemy import Column, Unicode, Integer, ForeignKey

from data.db.database import Model
from data.db.utils import make_id_column


class Template(Model):
    __tablename__ = 'template'

    id = make_id_column(autoincrement=True, seq_name='template_id_seq', model=Model)
    language = Column(Unicode(33), nullable=False)
    description = Column(Unicode(128), nullable=False)
    source = Column(Unicode(128), nullable=False)
    source_type = Column(Integer, nullable=False)
    difficulty = Column(Integer, nullable=False)


class TemplateStep(Model):
    __tablename__ = 'template_step'

    id = make_id_column(autoincrement=True, seq_name='template_step_id_seq', model=Model)
    template_id = Column(ForeignKey(Template.id, name='template_id_fkey'))
    description = Column(Unicode(128), nullable=False)
