from sqlalchemy import Column, Unicode, Integer, ForeignKey
from sqlalchemy.orm import relationship

from data.db.database import Model
from data.db.utils import make_id_column


class Resources(Model):
    __tablename__ = 'resources'

    id = make_id_column(Model, autoincrement=False)
    name = Column(Unicode(64), nullable=False)


class Permissions(Model):
    __tablename__ = 'permissions'

    id = make_id_column(autoincrement=True, seq_name='permissions_id_seq', model=Model)
    role_id = Column(
        'role_id',
        Integer,
        ForeignKey('roles.id', name='permissions_role_id_fk', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False
    )
    resource_id = Column(
        'resource_id',
        Integer,
        ForeignKey('resources.id', name='permissions_resource_id_fk', ondelete='CASCADE', onupdate='CASCADE'),
        nullable=False
    )


class Roles(Model):
    __tablename__ = 'roles'

    id = make_id_column(autoincrement=True, seq_name='roles_id_seq', model=Model)
    name = Column(Unicode(64), nullable=False)
    description = Column(Unicode(512))


class Users(Model):
    __tablename__ = 'users'

    id = make_id_column(autoincrement=True, seq_name='users_id_seq', model=Model)
    email = Column(Unicode(256), nullable=False, unique=True)
    name = Column(Unicode(64), nullable=False)
    surname = Column(Unicode(64), nullable=False)
    patronymic = Column(Unicode(64), nullable=False)
    password_hash = Column(Unicode(256), nullable=False)
    role_id = Column(ForeignKey(Roles.id, name='role_id_fkey'))

    role: Roles = relationship('Roles')
